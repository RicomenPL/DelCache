﻿using System;
using System.IO;
using System.Threading;

namespace DelCache
{
    class system6
    {
        public static bool work( bool otherworld)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string CASPartCache = "CASPartCache.package";
            string compositorCache = "compositorCache.package";
            string simCompositorCache = "simCompositorCache.package";
            string socialCache = "socialCache.package";
            string scriptCache = "scriptCache.package";
            string ts3direct = "Electronic Arts\\The Sims 3";
            string tmp = "DCCache";
            string thumbnails = "Thumbnails";
            string worldcaches = "WorldCaches";
            string pathts3direct = Path.Combine(path, ts3direct);
            string pathCASPartCache = Path.Combine(path, ts3direct, CASPartCache);
            string pathcompositorCache = Path.Combine(path, ts3direct, compositorCache);
            string pathsimCompositorCache = Path.Combine(path, ts3direct, simCompositorCache);
            string pathsocialCache = Path.Combine(path, ts3direct, socialCache);
            string pathscriptCache = Path.Combine(path, ts3direct, scriptCache);
            string pathtmp = Path.Combine(path, ts3direct, tmp);
            string paththumbnails = Path.Combine(path, ts3direct, thumbnails);
            string pathworldcaches = Path.Combine(path, ts3direct, worldcaches);

            Console.Clear();
            Console.WriteLine("Usuwanie cache gry");

            if (Directory.Exists(pathts3direct))
            {
                try
                {
                    File.Delete(@pathCASPartCache);
                    File.Delete(@pathcompositorCache);
                    File.Delete(@pathsimCompositorCache);
                    File.Delete(@pathsocialCache);
                    File.Delete(@pathscriptCache);
                }
                catch (IOException x)
                {
                    Console.WriteLine(x.Message);
                    return false;
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("DelCache {0}", version.info());
                Console.WriteLine();
                Console.WriteLine("Program nie wykrył folderu The Sims 3 w miejscu docelowym wymienionym poniżej");
                Console.WriteLine(pathts3direct);
                Thread.Sleep(10000);
                Environment.Exit(0);

            }

            string[] files = Directory.GetFiles(@pathtmp, "*.tmp");
            foreach (string file in files)
            {
                File.Delete(file);
            }

            DirectoryInfo baseDir = new DirectoryInfo(@paththumbnails);
            if (baseDir.Exists)
            {
                foreach (var file in baseDir.EnumerateFiles())
                {
                    if (file.Name != "DownloadsThumbnails.package")
                    {
                        File.Delete(file.FullName);
                    }
                }
            }

            if (otherworld == true)
            {
                Console.Clear();
                Console.WriteLine("DelCache {0}", version.info());
                Console.WriteLine();
                Console.WriteLine("Cache gry The Sims 3 został wyczyszczony. Dobrej zabawy w życie :)");
                Thread.Sleep(5000);
                Environment.Exit(0);
            }
            else
            {
                string[] filesworld = Directory.GetFiles(@pathworldcaches);
                foreach (string fileworld in filesworld)
                {
                    File.Delete(fileworld);
                }
                Console.Clear();
                Console.WriteLine("DelCache {0}", version.info());
                Console.WriteLine();
                Console.WriteLine("Cache gry The Sims 3 został wyczyszczony. Dobrej zabawy w życie :)");
                Thread.Sleep(5000);
                Environment.Exit(0);
            }
            return false;
        }
    }
}
